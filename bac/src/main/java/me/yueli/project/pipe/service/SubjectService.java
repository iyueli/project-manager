package me.yueli.project.pipe.service;

import me.yueli.project.pipe.entity.SubjectEntity;
import me.yueli.project.pipe.repository.SubjectRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hy
 */
@Service
public class SubjectService {

    @Resource
    private SubjectRepository subjectRepository;

    public SubjectEntity query(String account, String password) {
        return subjectRepository.queryByAccountAndPassword(account, password);
    }

    public List<SubjectEntity> list() {
        return subjectRepository.findAll();
    }

}
