package me.yueli.project.pipe.context;

/**
 * @author hy
 */
public class RequestContext {

    private final static ThreadLocal<RequestContext> REQUEST_CONTEXT_THREAD_LOCAL = new InheritableThreadLocal<>();

    public static final String TOKEN_NAME = "a-token";

    private Long sid;

    private boolean isAdmin;

    public static void setCurrentRequestContext(RequestContext requestContext) {
        REQUEST_CONTEXT_THREAD_LOCAL.set(requestContext);
    }

    public static void clear() {
        REQUEST_CONTEXT_THREAD_LOCAL.remove();
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
