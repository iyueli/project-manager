package me.yueli.project.pipe.entity.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author hy
 */
@MappedSuperclass
public class BaseWithModifyEntity<ID extends Serializable> extends BaseEntity<ID> {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "gmt_modify", insertable = false, updatable = true, columnDefinition = "datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP")
    private Date gmtModify;

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }
}
